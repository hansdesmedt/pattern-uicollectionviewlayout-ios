//
//  HomeCollectionViewLayoutAttributes.swift
//  Stievie
//
//  Created by Simon Salomons on 08/04/16.
//  Copyright © 2016 In The Pocket. All rights reserved.
//

import UIKit

class HomeCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
    var visibility: CGFloat = 1
    var fullScreen = false
    
    override func copy(with zone: NSZone?) -> Any {
        let newAttributes = super.copy(with: zone)
        (newAttributes as? HomeCollectionViewLayoutAttributes)?.visibility = visibility
        (newAttributes as? HomeCollectionViewLayoutAttributes)?.fullScreen = fullScreen
        return newAttributes
    }
}
