//
//  Constants.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

// swiftlint:disable force_unwrapping
struct Configuration {
    static let itemAspectRatio: CGFloat = 1.0 / 16.0 * 9.0
}