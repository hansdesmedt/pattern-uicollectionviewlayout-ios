//
//  InfiniteHomeCollectionViewDelegate.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

extension InfiniteHomeCollectionView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        infiniteDelegate?.didSelectCellAtIndexPath(self, dequeueIndexPath: indexPath, usableIndexPath: IndexPath(row: getCorrectedIndex(indexPath.row - indexOffset), section: 0))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        infiniteDelegate?.scrollViewDidEndDecelerating(scrollView)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        infiniteDelegate?.scrollViewDidEndScrollingAnimation(scrollView)
    }
}
