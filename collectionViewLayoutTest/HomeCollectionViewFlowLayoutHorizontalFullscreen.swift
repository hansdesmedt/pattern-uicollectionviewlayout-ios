//
//  CollectionViewFlowLayoutHorizontal.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 06/09/16.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionViewFlowLayoutHorizontalFullscreen: HomeCollectionViewFlowLayoutHorizontal {
    
    override var minimumLineSpacing: CGFloat {
        return 0
    }
    
    override var itemSize: CGSize {
        return size
    }
    
    override var xOffset: CGFloat {
        return 0
    }
    
    override var yOffset: CGFloat {
        return 0
    }
    
    override func getLayoutAttributesForItemAtIndex(_ index: Int, offset: CGPoint, layoutAttributes: HomeCollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes? {
        layoutAttributes.transform = CGAffineTransform.identity
        layoutAttributes.alpha = 1
        layoutAttributes.visibility = 1
        return layoutAttributes
    }
}
