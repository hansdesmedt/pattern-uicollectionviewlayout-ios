//
//  CollectionViewCell.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 01/09/16.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    fileprivate var lastSize = CGSize.zero

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {

        guard layoutAttributes is HomeCollectionViewLayoutAttributes else {
            return super.apply(layoutAttributes)
        }
        
        if !lastSize.equalTo(layoutAttributes.size) {
            lastSize = layoutAttributes.size

            //hack for setCollectionViewLayout to set layout correct
            UIView.animate(withDuration: 0, animations: {
                self.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func showModal(_ sender: UIButton) {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController {
            rootViewController.performSegue(withIdentifier: "ModalViewController", sender: sender)
        }
    }
}
