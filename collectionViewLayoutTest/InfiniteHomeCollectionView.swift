//
//  InfiniteHomeCollectionView.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

protocol InfiniteCollectionViewDataSource {
    func cellForItemAtIndexPath(_ collectionView: UICollectionView, dequeueIndexPath: IndexPath, usableIndexPath: IndexPath) -> UICollectionViewCell
    func numberOfItems(_ collectionView: UICollectionView) -> Int
}

protocol InfiniteCollectionViewDelegate {
    func didSelectCellAtIndexPath(_ collectionView: UICollectionView, dequeueIndexPath: IndexPath, usableIndexPath: IndexPath)
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
}

class InfiniteHomeCollectionView: HomeCollectionView {

    var infiniteDataSource: InfiniteCollectionViewDataSource?
    var infiniteDelegate: InfiniteCollectionViewDelegate?

    var indexOffset = 0

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        dataSource = self
        delegate = self
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !animatingLayout {
            centerIfNeeded()
        }
    }

    //MARK: offset
    
    func centerIfNeeded() {
        guard let wrapper = layout else { return }
        
        let collectionViewContentSize = wrapper.collectionViewContentSize
        if wrapper.scrollDirection == .vertical {
            calculateVerticalOffset(collectionViewContentSize.height, currentOffset: contentOffset)
        } else {
            calculateHorizontalOffset(collectionViewContentSize.width, currentOffset: contentOffset)
        }
    }

    
    fileprivate func calculateVerticalOffset(_ contentHeight: CGFloat, currentOffset: CGPoint) {
        guard let theLayout = layout?.layout else { return }
        
        // Calculate the centre of content X position offset and the current distance from that centre point
        let centerOffsetY: CGFloat = (contentHeight - bounds.size.height) / 2
        let distFromCentre = centerOffsetY - currentOffset.y
        
        if (fabs(distFromCentre) > (contentHeight / 4)) {
            // Total cells (including partial cells) from centre
            let cellcount = distFromCentre/theLayout.itemLength
            
            // Amount left over to correct for
            let offsetCorrection = (abs(cellcount).truncatingRemainder(dividingBy: 1)) * theLayout.itemLength
            
            // Scroll back to the centre of the view, offset by the correction to ensure it's not noticable
            if (contentOffset.y < centerOffsetY) {
                //left scrolling
                contentOffset = CGPoint(x: currentOffset.x, y: centerOffsetY - offsetCorrection)
            } else if (contentOffset.y > centerOffsetY) {
                //right scrolling
                contentOffset = CGPoint(x: currentOffset.x, y: centerOffsetY + offsetCorrection)
            }
            shiftCells(cellcount)
        }
    }
    
    fileprivate func calculateHorizontalOffset(_ contentWidth: CGFloat, currentOffset: CGPoint) {
        guard let theLayout = layout?.layout else { return }

        // Calculate the centre of content X position offset and the current distance from that centre point
        let centerOffsetX: CGFloat = (contentWidth - bounds.size.width) / 2
        let distFromCentre = centerOffsetX - currentOffset.x
        
        
        if (fabs(distFromCentre) > (contentWidth / 4)) {
            // Total cells (including partial cells) from centre
            let cellcount = distFromCentre/theLayout.itemLength
            
            // Amount left over to correct for
            let offsetCorrection = (abs(cellcount).truncatingRemainder(dividingBy: 1)) * theLayout.itemLength
            
            // Scroll back to the centre of the view, offset by the correction to ensure it's not noticable
            if (contentOffset.x < centerOffsetX) {
                //left scrolling
                contentOffset = CGPoint(x: centerOffsetX - offsetCorrection, y: currentOffset.y)
            } else if (contentOffset.x > centerOffsetX) {
                //right scrolling
                contentOffset = CGPoint(x: centerOffsetX + offsetCorrection, y: currentOffset.y)
            }
            shiftCells(cellcount)
        }
    }
    
    //MARK: index
    
    fileprivate func shiftCells(_ cellcount: CGFloat) {
        // Amount of cells to shift (whole number) - conditional statement due to nature of +ve or -ve cellcount
        let shiftCells = Int((cellcount > 0) ? floor(cellcount) : ceil(cellcount))
        // Make content shift as per shiftCells
        shiftContentArray(getCorrectedIndex(shiftCells))
        
        // Reload cells, due to data shift changes above
        reloadData()
    }
    
    fileprivate func shiftContentArray(_ offset: Int) {
        indexOffset += offset
    }

    func getCorrectedIndex(_ indexToCorrect: Int) -> Int {
        if let numberOfCells = infiniteDataSource?.numberOfItems(self) {
            if (indexToCorrect < numberOfCells && indexToCorrect >= 0) {
                return indexToCorrect
            } else {
                let countInIndex = Float(indexToCorrect) / Float(numberOfCells)
                let flooredValue = Int(floor(countInIndex))
                let offset = numberOfCells * flooredValue
                return indexToCorrect - offset
            }
        } else {
            return 0
        }
    }
}
