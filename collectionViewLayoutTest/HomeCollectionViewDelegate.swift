//
//  CollectionViewDelegate.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionViewDelegate: NSObject, UICollectionViewDelegate, InfiniteCollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let homeCollectionView = collectionView as? HomeCollectionView else { return }
        guard let layout = homeCollectionView.layout else { return }
        
        if layout.layoutState == .horizontalFullscreen {
            return
        } else if indexPath.item == layout.centerItemIndex {
            homeCollectionView.updateLayoutToFullscreen(true)
        } else {
            homeCollectionView.isUserInteractionEnabled = false
            homeCollectionView.scrollToIndexPath(indexPath)
        }
    }
    
    func didSelectCellAtIndexPath(_ collectionView: UICollectionView, dequeueIndexPath: IndexPath, usableIndexPath: IndexPath) {
        self.collectionView(collectionView, didSelectItemAt: dequeueIndexPath)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let homeCollectionView = scrollView as? HomeCollectionView else { return }
        homeCollectionView.isUserInteractionEnabled = true
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        guard let homeCollectionView = scrollView as? HomeCollectionView else { return }
        homeCollectionView.isUserInteractionEnabled = true
    }
}
