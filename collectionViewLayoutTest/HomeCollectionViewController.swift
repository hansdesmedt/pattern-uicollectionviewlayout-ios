//
//  CollectionViewController.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 01/09/16.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionViewController: UICollectionViewController {

    fileprivate var delegate = HomeCollectionViewDelegate()
    fileprivate var dataSource = HomeCollectionViewDataSource()

    @IBOutlet fileprivate weak var homeCollectionView: HomeCollectionView! {
        didSet {
            homeCollectionView.delegate = delegate
            homeCollectionView.dataSource = dataSource
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        // with a modal viewcontroller above, layout needs to be invalidated
        collectionView?.collectionViewLayout.invalidateLayout()
        // size will be the new size (rotated), better than using collectionView.frame (old size)
        homeCollectionView.viewWillTransitionToSize(size: size)
        coordinator.animate(alongsideTransition: nil) { _ in
            // with a modal viewcontroller above, this will trigger targetContentOffset:forProposedContentOffset
            // so with every rotation, contentoffset of collectionView will be right
            // this can be understood as: viewDidTransition
            self.collectionView?.performBatchUpdates(nil, completion: nil)
        }
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return isiPad() ? .landscape : .allButUpsideDown
    }
    
    @IBAction func unwindFromModal(_ segue: UIStoryboardSegue) {

    }
}
