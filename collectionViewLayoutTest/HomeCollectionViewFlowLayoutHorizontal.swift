//
//  CollectionViewFlowLayoutHorizontalSmall.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 07/09/16.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionViewFlowLayoutHorizontal: CollectionViewFlowLayout {
    
    var size = CGSize.zero
    var numberOfItems = 0
    
    var minimumLineSpacing: CGFloat {
        return (size.width - itemSize.width) / 10.0
    }
    
    var itemSize: CGSize {
        let width = size.width * 0.8
        return CGSize(width: width, height: width * Configuration.itemAspectRatio)
    }
    
    var itemLength: CGFloat {
        return itemSize.width + minimumLineSpacing
    }
    
    var xOffset: CGFloat {
        return (size.width - itemSize.width) / 2.0
    }
    
    var yOffset: CGFloat {
        return (size.height - itemSize.height) / 2.0
    }
    
    func getFrameForIndexPath(_ indexPath: IndexPath) -> CGRect {
        let x = xOffset + (itemLength * CGFloat(indexPath.item))
        
        return CGRect(x: x, y: yOffset, width: itemSize.width, height: itemSize.height)
    }
    
    func getCollectionViewContentSize(_ superContentSize: CGSize) -> CGSize {
        if numberOfItems > 0 {
            let width = (itemLength * CGFloat(numberOfItems)) - minimumLineSpacing + (xOffset * 2.0)
            let height = superContentSize.height
            
            return CGSize(width: width, height: height)
        }
        
        return CGSize.zero
    }
    
    func getLayoutAttributesForItemAtIndex(_ index: Int, offset: CGPoint, layoutAttributes: HomeCollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes? {
        let cellOffset = offset.x - (CGFloat(index) * itemLength)
        var offsetPercentage = abs(cellOffset) / itemLength
        if offsetPercentage.isNaN {
            offsetPercentage = 0
        }
        let scaleOffset = offsetPercentage / 8
        let scale = 1 - scaleOffset
        layoutAttributes.transform = CGAffineTransform(scaleX: scale, y: scale)
        layoutAttributes.alpha = 1 - (offsetPercentage * 0.8)
        layoutAttributes.visibility = 1 - offsetPercentage
        return layoutAttributes
    }
    
    func getTargetContentOffsetForCenterItem(_ centerItemIndex: Int) -> CGPoint {
        let offsetLength = CGFloat(centerItemIndex) * itemLength
        return CGPoint(x: offsetLength, y: 0)
    }
    
    func getTargetContentOffsetForVelocity(_ velocity: CGPoint, offset: CGPoint) -> CGPoint {
        let offset = offset.x
        let restOffset = offset.truncatingRemainder(dividingBy: itemLength)
        let velocity = velocity.x
        var targetOffset = offset - restOffset
        if velocity > 0 || (velocity == 0 && restOffset > itemLength / 2) {
            targetOffset += itemLength
        }
        return CGPoint(x: floor(targetOffset), y: 0)
    }
    
    func getCenterIndexForOffset(_ offset: CGPoint) -> Int {
        if numberOfItems > 0 {
            let index = Int((offset.x + (itemLength / 2)) / itemLength)
            return index > numberOfItems - 1 ? numberOfItems - 1 : index
        }
        
        return 0
    }
}
