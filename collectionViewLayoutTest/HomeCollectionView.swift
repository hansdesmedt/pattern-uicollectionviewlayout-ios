//
//  CollectionView.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionView: UICollectionView {

    var resignOrientation: UIDeviceOrientation?
    var foregroundOrientation: UIDeviceOrientation?
    var animatingLayout = false

    var layout: HomeCollectionViewFlowLayout? {
        return (collectionViewLayout as? HomeCollectionViewFlowLayout)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NotificationCenter.default.addObserver(self, selector: #selector(resignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

        setInterfaceOrientation()
        setLayoutScrollDirection()
    }
    
    func scrollToIndexPath(_ indexPath: IndexPath, animated: Bool = true) {
        scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: animated)
        layout?.centerItemIndex = indexPath.item
    }
    
    func viewWillTransitionToSize(size: CGSize) {
        if !isiPad() {
            layout?.size = size
            foregroundOrientation = UIDevice.current.orientation
            hackForRotatingAfterResignAndForeground()
            setLayoutScrollDirection()
        }
    }
    
    func updateLayoutToFullscreen(_ fullscreen: Bool) {
        if isiPad() {
            horizontalAnimateToFullscreen(fullscreen)
        } else {
            rotateDeviceTo(fullscreen)
        }
        //        UIView.animateWithDuration(0.3) {
        //            self.setNeedsStatusBarAppearanceUpdate()
        //        }
    }
    
    fileprivate dynamic func resignActive() {
        resignOrientation = UIDevice.current.orientation
    }

    fileprivate func setInterfaceOrientation() {
        let orientation = UIApplication.shared.statusBarOrientation
        if orientation == UIInterfaceOrientation.landscapeLeft {
            foregroundOrientation = .landscapeLeft
        } else if orientation == UIInterfaceOrientation.landscapeRight {
            foregroundOrientation = .landscapeRight
        } else if orientation == UIInterfaceOrientation.portrait {
            foregroundOrientation = .portrait
        } else if orientation == UIInterfaceOrientation.portraitUpsideDown {
            foregroundOrientation = .portraitUpsideDown
        }
    }

    fileprivate func hackForRotatingAfterResignAndForeground() {
        guard let foregroundOrientation = foregroundOrientation, let resignOrientation = resignOrientation, let layout = layout else { return }
        let bounds = UIScreen.main.bounds
        let size = CGSize(width: bounds.height, height: bounds.width)
        if UIDeviceOrientationIsLandscape(resignOrientation) && UIDeviceOrientationIsPortrait(foregroundOrientation) {
            layout.setLayoutToVertical(size, animated: false)
        } else if UIDeviceOrientationIsLandscape(foregroundOrientation) && UIDeviceOrientationIsPortrait(resignOrientation) {
            layout.setLayoutToHorizontalFullscreen(size, animated: false)
        }
        self.resignOrientation = nil
    }

    fileprivate func horizontalAnimateToFullscreen(_ fullscreen: Bool) {
        guard let layout = layout else { return }
        let newLayout = HomeCollectionViewFlowLayout()
        if fullscreen {
            
            newLayout.layoutState = .horizontalFullscreen
        } else {
            newLayout.layoutState = .horizontal
        }
        newLayout.centerItemIndex = layout.centerItemIndex
        animatingLayout = true
        setCollectionViewLayout(newLayout, animated: true)
        animatingLayout = false
    }

    fileprivate func rotateDeviceTo(_ fullscreen: Bool) {
        // This doesn't actually set the layout to fullscreen, but rotates the device orientation. Which in his turn tells the layout to go to fullscreen
        let newOrientation: UIInterfaceOrientation = fullscreen ? .landscapeLeft : .portrait
        UIDevice.current.setValue(newOrientation.rawValue, forKey: "orientation")
    }

    fileprivate func setLayoutScrollDirection() {
        guard let foregroundOrientation = foregroundOrientation, let layout = layout else { return }
        if isiPad() {
            layout.layoutState = .horizontal
        } else if UIDeviceOrientationIsLandscape(foregroundOrientation) {
            layout.layoutState = .horizontalFullscreen
        } else {
            layout.layoutState = .vertical
        }
    }
}
