//
//  CollectionViewLayout2.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 02/09/16.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

protocol CollectionViewFlowLayout: class {
    var minimumLineSpacing: CGFloat { get }
    var size: CGSize { get set }
    var numberOfItems: Int { get set }
    var itemSize: CGSize { get }
    var itemLength: CGFloat { get }
    func getFrameForIndexPath(_ indexPath: IndexPath) -> CGRect
    func getCollectionViewContentSize(_ superContentSize: CGSize) -> CGSize
    func getLayoutAttributesForItemAtIndex(_ index: Int, offset: CGPoint, layoutAttributes: HomeCollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes?
    func getTargetContentOffsetForCenterItem(_ centerItemIndex: Int) -> CGPoint
    func getTargetContentOffsetForVelocity(_ velocity: CGPoint, offset: CGPoint) -> CGPoint
    func getCenterIndexForOffset(_ offset: CGPoint) -> Int
}

enum LayoutState: Int {
    case vertical
    case horizontal
    case horizontalFullscreen
}

class HomeCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    var centerItemIndex: Int = 0
    
    var previousLayoutState: LayoutState = .vertical
    var layoutState: LayoutState = .vertical {
        willSet {
            previousLayoutState = layoutState
        }
        didSet {
            scrollDirection = (layoutState == .vertical) ? .vertical : .horizontal
            invalidateLayout()
        }
    }
    
    var size: CGSize?
    
    fileprivate var layoutInfo: [IndexPath:UICollectionViewLayoutAttributes] = [:]
    
    fileprivate var layoutHorizontalFullscreen = HomeCollectionViewFlowLayoutHorizontalFullscreen()
    fileprivate var layoutHorizontal = HomeCollectionViewFlowLayoutHorizontal()
    fileprivate var layoutVertical = HomeCollectionViewFlowLayoutVertical()
    
    var layout: CollectionViewFlowLayout {
        switch layoutState {
        case .vertical:
            return layoutVertical
        case .horizontal:
            return layoutHorizontal
        case .horizontalFullscreen:
            return layoutHorizontalFullscreen
        }
    }
    
    override func prepare() {
        guard let collectionView = collectionView else { return }
        
        layout.numberOfItems = collectionView.numberOfItems(inSection: 0)
        if let size = size {
            layout.size = size
        } else {
            layout.size = collectionView.frame.size
        }
        
        minimumInteritemSpacing = 0
        minimumLineSpacing = layout.minimumLineSpacing
        itemSize = layout.itemSize
        
        super.prepare()
        
        layoutInfo = getLayoutInfo()
    }
    
    override var collectionViewContentSize: CGSize {
        let contentSize = super.collectionViewContentSize
        return layout.getCollectionViewContentSize(contentSize)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var allAttributes: [UICollectionViewLayoutAttributes] = [UICollectionViewLayoutAttributes]()
        for (indexPath, attributes) in layoutInfo {
            if rect.intersects(attributes.frame) {
                if let attributes = layoutAttributesForItem(at: indexPath) {
                    allAttributes.append(attributes)
                }
            }
        }
        return allAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let layoutAttributes = layoutInfo[indexPath], let collectionView = collectionView else { return nil }
        
        let homeLayoutAttributes = HomeCollectionViewLayoutAttributes(forCellWith: indexPath)
        homeLayoutAttributes.frame = layoutAttributes.frame
        homeLayoutAttributes.fullScreen = (layoutState == .horizontalFullscreen)
        
        let index = collectionView.itemIndexForIndexPath(indexPath)
        let offset = collectionView.contentOffset
        
        return layout.getLayoutAttributesForItemAtIndex(index, offset: offset, layoutAttributes: homeLayoutAttributes)
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if isiPad() {
            return nil
        }
        return super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if isiPad() {
            guard let layoutAttributes = layoutInfo[itemIndexPath], let collectionView = collectionView else { return nil }
            
            let homeLayoutAttributes = HomeCollectionViewLayoutAttributes(forCellWith: itemIndexPath)
            homeLayoutAttributes.frame = layoutAttributes.frame
            homeLayoutAttributes.fullScreen = (layoutState == .horizontalFullscreen)
            
            let index = collectionView.itemIndexForIndexPath(itemIndexPath)
            let offset = collectionView.contentOffset
            
            if previousLayoutState == .horizontal {
                return layoutHorizontalFullscreen.getLayoutAttributesForItemAtIndex(index, offset: offset, layoutAttributes: homeLayoutAttributes)
            }
            
            if previousLayoutState == .horizontalFullscreen {
                return layoutHorizontal.getLayoutAttributesForItemAtIndex(index, offset: offset, layoutAttributes: homeLayoutAttributes)
            }
            
            return layout.getLayoutAttributesForItemAtIndex(index, offset: offset, layoutAttributes: homeLayoutAttributes)
        }
        return super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath)
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return layout.getTargetContentOffsetForCenterItem(centerItemIndex)
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else { return proposedContentOffset }
        
        let contentOffset = collectionView.contentOffset
        let offset = layout.getTargetContentOffsetForVelocity(velocity, offset: contentOffset)
        centerItemIndex = layout.getCenterIndexForOffset(offset)
        return offset
        
    }
    
    fileprivate func getLayoutInfo() -> [IndexPath:UICollectionViewLayoutAttributes] {
        var layoutInfo = [IndexPath: UICollectionViewLayoutAttributes]()
        
        guard let collectionView = collectionView, collectionView.numberOfItems(inSection: 0) - 1 >= 0 else { return layoutInfo }
        for i in 0...collectionView.numberOfItems(inSection: 0) - 1 {
            let indexPath = IndexPath(row: i, section: 0)
            let itemAttributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            itemAttributes.frame = layout.getFrameForIndexPath(indexPath)
            layoutInfo[indexPath] = itemAttributes
        }
        return layoutInfo
    }
    
    // Enforce layout content size & content offset
    
    func setLayoutToVertical(_ size: CGSize, animated: Bool) {
        setLayoutTo(layoutVertical, size: size, animated: animated)
    }
    
    func setLayoutToHorizontal(_ size: CGSize, animated: Bool) {
        setLayoutTo(layoutHorizontal, size: size, animated: animated)
    }
    
    func setLayoutToHorizontalFullscreen(_ size: CGSize, animated: Bool) {
        setLayoutTo(layoutHorizontalFullscreen, size: size, animated: animated)
    }
    
    fileprivate func setLayoutTo(_ layout: CollectionViewFlowLayout, size: CGSize, animated: Bool) {
        guard let collectionView = collectionView else { return }
        
        layout.size = size
        collectionView.contentSize = layout.getCollectionViewContentSize(size)
        let offset = layout.getTargetContentOffsetForCenterItem(centerItemIndex)
        collectionView.setContentOffset(offset, animated: animated)
    }
}
