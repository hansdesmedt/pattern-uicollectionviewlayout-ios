//
//  ModalViewController.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 10/08/2017.
//  Copyright © 2017 Hans De Smedt. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .allButUpsideDown
    }
}
