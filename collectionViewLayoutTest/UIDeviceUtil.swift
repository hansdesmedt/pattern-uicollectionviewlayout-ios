//
//  UIDeviceUtil.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import Foundation
import UIKit

func isiPad() -> Bool {
    return UIDevice.current.userInterfaceIdiom == .pad
}
