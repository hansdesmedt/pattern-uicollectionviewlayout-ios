//
//  ColorsDataSource.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 23/09/2016.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class HomeCollectionViewDataSource: NSObject, UICollectionViewDataSource, InfiniteCollectionViewDataSource {

    fileprivate var colors = [ UIColor.blue, UIColor.brown, UIColor.cyan, UIColor.darkGray, UIColor.green, UIColor.magenta, UIColor.orange, UIColor.purple, UIColor.red, UIColor.white]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeCollectionViewCell
        cell.backgroundColor = colors[indexPath.item]
        cell.label.text = String(indexPath.item)
        return cell
    }
    
    func cellForItemAtIndexPath(_ collectionView: UICollectionView, dequeueIndexPath: IndexPath, usableIndexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView(collectionView, cellForItemAt: usableIndexPath)
    }
    
    func numberOfItems(_ collectionView: UICollectionView) -> Int {
        return colors.count
    }
}
