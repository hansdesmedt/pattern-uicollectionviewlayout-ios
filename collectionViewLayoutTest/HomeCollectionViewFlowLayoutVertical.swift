//
//  CollectionViewFlowLayoutVertical.swift
//  collectionViewLayoutTest
//
//  Created by Hans De Smedt on 06/09/16.
//  Copyright © 2016 Hans De Smedt. All rights reserved.
//

import UIKit

class HomeCollectionViewFlowLayoutVertical: CollectionViewFlowLayout {

    var size = CGSize.zero
    var numberOfItems = 0
    
    var minimumLineSpacing: CGFloat = 20

    var itemSize: CGSize {
        let width = size.width
        return CGSize(width: width, height: width * Configuration.itemAspectRatio)
    }

    fileprivate var startOffset: CGFloat {
        return (size.height - itemSize.height) / 2.0
    }

    var itemLength: CGFloat {
        return itemSize.height + minimumLineSpacing
    }

    func getFrameForIndexPath(_ indexPath: IndexPath) -> CGRect {
        let ypos = itemLength * CGFloat(indexPath.item)
        return CGRect(x: 0, y: ypos + startOffset, width: itemSize.width, height: itemSize.height)
    }

    func getCollectionViewContentSize(_ superContentSize: CGSize) -> CGSize {
        let height = itemLength * CGFloat(numberOfItems) - minimumLineSpacing + startOffset * 2.0
        let width = superContentSize.width
        return CGSize(width: width, height: height)
    }

    func getLayoutAttributesForItemAtIndex(_ index: Int, offset: CGPoint, layoutAttributes: HomeCollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes? {
        let cellOffset = offset.y - (CGFloat(index) * itemLength)
        let offsetPercentage = abs(cellOffset) / itemLength
        let scaleOffset = offsetPercentage / 8
        let scale = 1 - scaleOffset
        layoutAttributes.transform = CGAffineTransform(scaleX: scale, y: scale)
        layoutAttributes.alpha = 1 - (offsetPercentage * 0.8)
        layoutAttributes.visibility = 1 - offsetPercentage
        return layoutAttributes
    }

    func getTargetContentOffsetForCenterItem(_ centerItemIndex: Int) -> CGPoint {
        let offsetLength = CGFloat(centerItemIndex) * itemLength
        return CGPoint(x: 0, y: offsetLength)
    }

    func getTargetContentOffsetForVelocity(_ velocity: CGPoint, offset: CGPoint) -> CGPoint {
        let offset = offset.y
        let restOffset = offset.truncatingRemainder(dividingBy: itemLength)
        let velocity = velocity.y
        var targetOffset = offset - restOffset
        if velocity > 0 || (velocity == 0 && restOffset > itemLength / 2) {
            targetOffset += itemLength
        }
        return CGPoint(x: 0, y: floor(targetOffset))
    }

    func getCenterIndexForOffset(_ offset: CGPoint) -> Int {
        let index = Int((offset.y + (itemLength / 2)) / itemLength)
        print(offset.y)
        return index > numberOfItems - 1 ? numberOfItems - 1 : index
    }
}
